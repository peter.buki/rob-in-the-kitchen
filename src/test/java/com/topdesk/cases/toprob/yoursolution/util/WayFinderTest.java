package com.topdesk.cases.toprob.yoursolution.util;

import com.topdesk.cases.toprob.Grid;
import com.topdesk.cases.toprob.Instruction;
import com.topdesk.cases.toprob.helper.GridFactory;
import org.junit.Test;

import java.util.List;

public class WayFinderTest {
    private Grid grid;
    private WayFinder wayFinder;
    private List<Instruction> instructionList;

    private void buildWayFinder() {
        wayFinder = new WayFinderBuilder().
                withGrid(grid).
                withTimeLimit(100).
//                withComplicated().
                build();
    }

    @Test
    public void performance_test_3x3() {
        grid = GridFactory.create(
                "r..",
                ".AB",
                "..k");
        buildWayFinder();
        runWayFinder();
    }

    private void runWayFinder() {
        instructionList = wayFinder.findTheWay(grid.getRoom(), grid.getKitchen(), 0);
    }

    @Test
    public void performance_test_4x4() {
        grid = GridFactory.create(
                "r...",
                ".AB.",
                "....",
                "..k.");
        buildWayFinder();
        runWayFinder();
    }

    @Test
    public void performance_test_5x5() {
        grid = GridFactory.create(
                "r....",
                ".AB..",
                ".....",
                ".....",
                "..k..");
        buildWayFinder();
        runWayFinder();
    }

    @Test
    public void performance_test_6x6() {
        grid = GridFactory.create(
                "r.....",
                ".AB...",
                "......",
                "......",
                "......",
                "..k...");
        buildWayFinder();
        runWayFinder();
    }

    @Test
    public void performance_test_7x7() {
        grid = GridFactory.create(
                "r......",
                ".AB....",
                ".......",
                ".......",
                ".......",
                ".......",
                "..k....");
        buildWayFinder();
        runWayFinder();
    }

    @Test
    public void performance_test_8x8() {
        grid = GridFactory.create(
                "r.......",
                ".AB.....",
                "........",
                "........",
                "........",
                "........",
                "........",
                "..k.....");
        buildWayFinder();
        runWayFinder();
    }

}