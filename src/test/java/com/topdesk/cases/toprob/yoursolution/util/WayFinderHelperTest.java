package com.topdesk.cases.toprob.yoursolution.util;

import com.topdesk.cases.toprob.Coordinate;
import com.topdesk.cases.toprob.Instruction;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class WayFinderHelperTest {
    @Test
    public void getInstructionsInOrder_3x3Grid_CorrectOrder() {
        Coordinate startPosition = new Coordinate(10, 10);
        Coordinate destinationPosition = new Coordinate(1, 1);
        // Order should be WEST, NORTH, PAUSE, EAST, SOUTH
        List<Instruction> instructions = WayFinderHelper.getInstructionsInOrder(startPosition, destinationPosition);
        assertEquals(5, instructions.size());
        assertEquals(Instruction.WEST, instructions.get(0));
        assertEquals(Instruction.NORTH, instructions.get(1));
        assertEquals(Instruction.PAUSE, instructions.get(2));
        assertEquals(Instruction.EAST, instructions.get(3));
        assertEquals(Instruction.SOUTH, instructions.get(4));
    }
}