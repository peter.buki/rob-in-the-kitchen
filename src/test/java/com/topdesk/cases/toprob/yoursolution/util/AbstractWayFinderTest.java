package com.topdesk.cases.toprob.yoursolution.util;

import com.topdesk.cases.toprob.Coordinate;
import com.topdesk.cases.toprob.Instruction;
import com.topdesk.cases.toprob.helper.GridFactory;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class AbstractWayFinderTest extends AbstractWayFinder {

    @Before
    public void setup() {
        this.grid = GridFactory.create("rkABo");
    }

    @Test
    public void isSolutionTooLong_yesItsOverTheLimit_returnsTrue() {
        List<Instruction> instructionList = new LinkedList<>();
        timeLimit = 0;
        assertEquals(true, isSolutionTooLong(instructionList));
    }

    @Test
    public void isSolutionTooLong_noItsNot_returnsFalse() {
        List<Instruction> instructionList = new LinkedList<>();
        timeLimit = 1;
        assertEquals(false, isSolutionTooLong(instructionList));
    }

    @Test
    public void isCoordinateOnTheGrid_yes_returnsTrue() {
        grid = getGrid();
        Coordinate coordinate = new Coordinate(0, 0);
        assertEquals(true, isCoordinateOnTheGrid(coordinate));
    }

    @Test
    public void isCoordinateOnTheGrid_negativeCoordinate_returnsFalse() {
        grid = getGrid();
        Coordinate coordinate = new Coordinate(-1, 0);
        assertEquals(false, isCoordinateOnTheGrid(coordinate));
    }

    @Test
    public void isCoordinateOnTheGrid_offGridXCoordinate_returnsFalse() {
        Coordinate coordinate = new Coordinate(5, 0);
        assertEquals(false, isCoordinateOnTheGrid(coordinate));
    }

    @Test
    public void isCoordinateOnTheGrid_offGridYCoordinate_returnsFalse() {
        grid = getGrid();
        Coordinate coordinate = new Coordinate(3, 2);
        assertEquals(false, isCoordinateOnTheGrid(coordinate));
    }

    @Test
    public void isCoordinateBugFree_yes_returnsTrue() {
        Coordinate coordinate = new Coordinate(2, 0);
        assertEquals(true, isCoordinateBugFree(coordinate, 1));
    }

    @Test
    public void isCoordinateBugFree_no_returnsFalse() {
        Coordinate coordinate = new Coordinate(2, 0);
        assertEquals(true, isCoordinateBugFree(coordinate, 1));
    }

    @Test
    public void isCoordinateHoleFree_yes_returnsTrue() {
        Coordinate coordinate = new Coordinate(2, 0);
        assertEquals(true, isCoordinateHoleFree(coordinate));
    }

    @Test
    public void isCoordinateHoleFree_no_returnsFalse() {
        Coordinate coordinate = new Coordinate(4, 0);
        assertEquals(false, isCoordinateHoleFree(coordinate));
    }

    // this method is needed here because this class extends the AbstractWayFinder class
    @Override
    public List<Instruction> findTheWay(Coordinate startCoordinate, Coordinate destinationCoordinate, int startTime) {
        return null;
    }
}