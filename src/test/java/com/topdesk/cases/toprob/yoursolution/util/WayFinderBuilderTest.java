package com.topdesk.cases.toprob.yoursolution.util;

import com.topdesk.cases.toprob.Grid;
import com.topdesk.cases.toprob.helper.GridFactory;
import com.topdesk.cases.toprob.yoursolution.util.WayFinder;
import com.topdesk.cases.toprob.yoursolution.util.WayFinderBuilder;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class WayFinderBuilderTest {
    private Grid grid;
    private int timeLimit;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setup() {
        grid = GridFactory.create("rkAB");
        timeLimit = 123;
    }

    @Test
    public void build_with_grid_and_time_limit() {
        WayFinder wayFinder = new WayFinderBuilder().withGrid(grid).withTimeLimit(timeLimit).build();
        assertEquals(grid, wayFinder.getGrid());
        assertEquals(timeLimit, wayFinder.getTimeLimit());
    }

    @Test
    public void build_without_grid_throws_exception() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("You must use a Grid to create a WayFinder!");
        new WayFinderBuilder().withTimeLimit(timeLimit).build();
    }

    @Test
    public void build_without_time_limit_throws_exception() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("You must specify a time limit!");
        new WayFinderBuilder().withGrid(grid).build();
    }
}