package com.topdesk.cases.toprob.yoursolution.util;

import com.topdesk.cases.toprob.Coordinate;
import com.topdesk.cases.toprob.Grid;
import com.topdesk.cases.toprob.Instruction;

import java.util.List;

public abstract class AbstractWayFinder implements WayFinder {
    protected Grid grid;
    protected int timeLimit;

    public Grid getGrid() {
        return grid;
    }

    public int getTimeLimit() {
        return timeLimit;
    }

    /**
     * This method checks the list of steps against the time limit set
     *
     * @param instructionList the {@link List} of {@link Instruction} to check
     * @return true if the solution is over the time limit, false otherwise
     */
    protected boolean isSolutionTooLong(List<Instruction> instructionList) {
        return instructionList.size() >= timeLimit;
    }

    /**
     * Checks if the coordinate is on the grid.
     *
     * @param coordinate to be checked
     * @return true, if it's on the grid
     */
    protected boolean isCoordinateOnTheGrid(Coordinate coordinate) {
        return 0 <= coordinate.getX() && coordinate.getX() < grid.getWidth()
                && 0 <= coordinate.getY() && coordinate.getY() < grid.getHeight();
    }

    /**
     * Checks if the coordinate is bug-free at a given time.
     *
     * @param coordinate to be checked for bug
     * @param time       when the coordinate is to be checked at
     * @return true if there is no bug at the time
     */
    protected boolean isCoordinateBugFree(Coordinate coordinate, int time) {
        return !grid.getBug(time).equals(coordinate);
    }

    /**
     * Checks if a given coordinate is not a hole.
     *
     * @param coordinate to be checked
     * @return true if there is no hole
     */
    protected boolean isCoordinateHoleFree(Coordinate coordinate) {
        return !grid.getHoles().contains(coordinate);
    }

}
