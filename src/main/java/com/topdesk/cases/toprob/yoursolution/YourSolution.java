package com.topdesk.cases.toprob.yoursolution;

import com.topdesk.cases.toprob.Grid;
import com.topdesk.cases.toprob.Instruction;
import com.topdesk.cases.toprob.Solution;
import com.topdesk.cases.toprob.yoursolution.util.WayFinder;
import com.topdesk.cases.toprob.yoursolution.util.WayFinderBuilder;

import java.util.List;

public class YourSolution implements Solution {
    private static final int TIME_LIMIT = 100;
    private static final int TIME_TO_SPEND_IN_KITCHEN = 5;

    /**
     * The solution for Rob's safe way to the kitchen and back.
     * It is close to optimal, but it is not: the way time limit is handled is far from perfect.
     * The reason for that is that the underlying algorithm is not looking for the shortest path for the whole way.
     *
     * When the way back to the room is much longer than the way to the kitchen, it can happen that
     * we spend a lot of time with going to the kitchen, while there is a shorter way.
     * Then, we run out of time on our way back.
     *
     * @param grid the grid to find a solution for
     * @param time the time that Rob starts his journey. This value is bigger
     *             than or equals to zero
     * @return
     */
    @Override
    public List<Instruction> solve(Grid grid, int time) {
        checkGrid(grid);
        checkStartTime(time);

        // Build a WayFinder with the given grid and time limit
        WayFinder wayFinder = new WayFinderBuilder().withGrid(grid).withTimeLimit(TIME_LIMIT).build();

        // Get the instructions for getting to the kitchen
        List<Instruction> wayToTheKitchenAndBack = wayFinder.findTheWay(grid.getRoom(), grid.getKitchen(), time);

        // Create a sandwich in the kitchen
        spendSomeTimeInTheKitchen(wayToTheKitchenAndBack);

        // Find our way back to the safety of the room
        List<Instruction> wayBackToTheRoom = wayFinder.findTheWay(grid.getKitchen(), grid.getRoom(),
                time + wayToTheKitchenAndBack.size());
        wayToTheKitchenAndBack.addAll(wayBackToTheRoom);

        return wayToTheKitchenAndBack;
    }

    /**
     * We need to spend some seconds in the kitchen to create a sandwich
     *
     * @param instructionList to be extended with the steps
     */
    private void spendSomeTimeInTheKitchen(List<Instruction> instructionList) {
        for (int seconds = 1; seconds <= TIME_TO_SPEND_IN_KITCHEN; seconds++) {
            instructionList.add(Instruction.PAUSE);
        }
    }

    /**
     * Checks the start time - it should be zero or greater than zero
     *
     * @param time the starting time to be checked
     */
    private void checkStartTime(int time) {
        if (time < 0) {
            throw new IllegalArgumentException(String.format("Invalid start time: %d", time));
        }
    }

    /**
     * Check the grid if it's valid.
     *
     * @param grid (@link Grid)
     */
    private void checkGrid(Grid grid) {
        grid.getRoom();
    }
}
