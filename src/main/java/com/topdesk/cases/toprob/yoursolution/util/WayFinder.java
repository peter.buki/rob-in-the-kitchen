package com.topdesk.cases.toprob.yoursolution.util;

import com.topdesk.cases.toprob.Coordinate;
import com.topdesk.cases.toprob.Grid;
import com.topdesk.cases.toprob.Instruction;

import java.util.List;

public interface WayFinder {
    int getTimeLimit();
    Grid getGrid();
    List<Instruction> findTheWay(Coordinate startCoordinate, Coordinate destinationCoordinate, int startTime);
}
