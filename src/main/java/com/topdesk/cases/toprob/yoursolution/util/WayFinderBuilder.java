package com.topdesk.cases.toprob.yoursolution.util;

import com.topdesk.cases.toprob.Grid;

public class WayFinderBuilder {
    private Grid grid;
    private int timeLimit = 0;
    private boolean simpleWayFinder = true;

    public WayFinderBuilder withGrid(Grid grid) {
        this.grid = grid;
        return this;
    }

    public WayFinderBuilder withTimeLimit(int timeLimit) {
        this.timeLimit = timeLimit;
        return this;
    }

    public WayFinderBuilder withComplicated() {
        simpleWayFinder = false;
        return this;
    }

    public WayFinder build() throws InstantiationError {
        if (null == grid) {
            throw new IllegalArgumentException("You must use a Grid to create a WayFinder!");
        }
        if (0 == timeLimit) {
            throw new IllegalArgumentException("You must specify a time limit!");
        }

        WayFinder wayFinder;
        if (simpleWayFinder) {
            wayFinder = new SimpleWayFinder();
            ((SimpleWayFinder) wayFinder).setGrid(grid);
            ((SimpleWayFinder) wayFinder).setTimeLimit(timeLimit);
        }
        else {
            wayFinder = new ComplicatedWayFinder();
            ((ComplicatedWayFinder) wayFinder).setGrid(grid);
            ((ComplicatedWayFinder) wayFinder).setTimeLimit(timeLimit);
        }
        return wayFinder;
    }
}
