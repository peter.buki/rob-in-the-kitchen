package com.topdesk.cases.toprob.yoursolution.util;

import com.topdesk.cases.toprob.Coordinate;
import com.topdesk.cases.toprob.Instruction;

import java.util.*;

public class ComplicatedWayFinder extends SimpleWayFinder {

    /**
     * The method finds a way between two coordinates within the given start time
     *
     * @param startCoordinate       the starting point in the grid
     * @param destinationCoordinate the destination coordinate in the grid
     * @param startTime             number of steps already committed
     * @return list of {@link Instruction} between the two coordinates
     */
    @Override
    public List<Instruction> findTheWay(Coordinate startCoordinate, Coordinate destinationCoordinate, int startTime) {
        positionsWeHaveAlreadyBeen = new HashSet<>();
        LinkedList<Instruction> instructionList = findTheBestWay(startCoordinate, destinationCoordinate, startTime, positionsWeHaveAlreadyBeen);
        System.out.println();
        for (Instruction instruction : instructionList ) {
            System.out.println("Instruction: " + instruction.name());
        }
        return instructionList;
    }

    private LinkedList<Instruction> findTheBestWay(Coordinate startCoordinate,
                                                   Coordinate destinationCoordinate,
                                                   int currentTime,
                                                   Set<Coordinate> placesAlreadyVisited) {
        //System.out.println(String.format("Current distance is: %d", getDistance(startCoordinate, destinationCoordinate)));
        if (startCoordinate.equals(destinationCoordinate)) {
            return new LinkedList<>();
        }

        if (currentTime >= timeLimit) {
            return null;
        }
        // we get the instructions in optimal order
        List<Instruction> instructionsInOrder = WayFinderHelper
                .getInstructionsInOrder(startCoordinate, destinationCoordinate);

        Map<Instruction, LinkedList<Instruction>> instructionListMap = new HashMap<>();
        int nextTime = currentTime + 1;

        for (Instruction instruction : instructionsInOrder) {
            Coordinate nextCoordinate = instruction.execute(startCoordinate);

            if ((!placesAlreadyVisited.contains(nextCoordinate)) &&
                    isCoordinateOnTheGrid(nextCoordinate) &&
                    isCoordinateBugFree(nextCoordinate, nextTime) &&
                    isCoordinateHoleFree(nextCoordinate)) {
                placesAlreadyVisited.add(nextCoordinate);
                LinkedList<Instruction> instructionList = findTheBestWay(nextCoordinate, destinationCoordinate, nextTime, placesAlreadyVisited);
                placesAlreadyVisited.remove(nextCoordinate);
                if ( null != instructionList ) {
                    instructionListMap.put(instruction, instructionList);
                }
            }
        }
        if (instructionListMap.isEmpty()) {
            return null;
        }
        LinkedList<Instruction> shortestPath = instructionListMap.values().iterator().next();
        Instruction shortestInstruction = instructionListMap.keySet().iterator().next();
        for (Instruction instruction : instructionListMap.keySet() ) {
            if (instructionListMap.get(instruction).size() < shortestPath.size()) {
                placesAlreadyVisited.add(instruction.execute(startCoordinate));
                shortestInstruction = instruction;
                shortestPath = instructionListMap.get(instruction);
            }
        }
        shortestPath.addFirst(shortestInstruction);
        return shortestPath;

    }


}
