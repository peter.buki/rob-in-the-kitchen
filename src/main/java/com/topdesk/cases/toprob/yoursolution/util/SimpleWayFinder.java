package com.topdesk.cases.toprob.yoursolution.util;

import com.topdesk.cases.toprob.Coordinate;
import com.topdesk.cases.toprob.Grid;
import com.topdesk.cases.toprob.Instruction;

import java.util.*;


public class SimpleWayFinder extends AbstractWayFinder {
    protected Set<Coordinate> positionsWeHaveAlreadyBeen;

    /**
     * Default constructor, to be used within the builder only
     */
    SimpleWayFinder() {
    }

    /**
     * Getter for the grid, used in unit test only
     *
     * @return the {@link Grid}
     */
    public Grid getGrid() {
        return grid;
    }

    /**
     * Setter for the {@link Grid}
     *
     * @param grid the Grid for finding a way in
     */
    void setGrid(Grid grid) {
        this.grid = grid;
    }

    /**
     * Getter for the time limit, used in unit test only
     *
     * @return timeLimit the maximum number of steps
     */
    public int getTimeLimit() {
        return timeLimit;
    }

    /**
     * Setter for the time limit. We must find a way within this many steps
     *
     * @param timeLimit the maximum number of steps
     */
    void setTimeLimit(int timeLimit) {
        this.timeLimit = timeLimit;
    }

    /**
     * The method finds a way between two coordinates within the given start time
     *
     * @param startCoordinate       the starting point in the grid
     * @param destinationCoordinate the destination coordinate in the grid
     * @param startTime             number of steps already committed
     * @return list of {@link Instruction} between the two coordinates
     */
    @Override
    public List<Instruction> findTheWay(Coordinate startCoordinate, Coordinate destinationCoordinate, int startTime) {
        LinkedList<Instruction> instructionList = new LinkedList<>();
        positionsWeHaveAlreadyBeen = new HashSet<>();
        findTheWay(instructionList, startCoordinate, destinationCoordinate, startTime);
        return instructionList;
    }

    /**
     * Recursive method for finding the way between two coordinates.
     * The WayFinder handles each way independently and it is not looking for the shortest way.
     * This might lead to problems with very special and thus very rare grids.
     *
     * @param instructionList       the list of {@link Instruction} with the steps already done
     * @param startCoordinate       the {@link Coordinate} for the current position
     * @param destinationCoordinate the {@link Coordinate} for the position to reach
     * @param timeElapsed           the current time for checking the {@link Grid} for bugs
     * @return true if we found a short way, without a bug or hole, false otherwise
     * @see com.topdesk.cases.toprob.yoursolution.YourSolution#solve(Grid, int)
     */
    private boolean findTheWay(LinkedList<Instruction> instructionList,
                               Coordinate startCoordinate,
                               Coordinate destinationCoordinate,
                               int timeElapsed) {
        // Rob, we have reached our target position, enjoy your meal!
        if (startCoordinate.equals(destinationCoordinate)) {
            return true;
        }

        // We ran over time limit, this way is too long, we failed to find it
        if (isSolutionTooLong(instructionList)) {
            return false;
        }

        // we get the instructions in optimal order
        List<Instruction> instructionsInOrder = WayFinderHelper
                .getInstructionsInOrder(startCoordinate, destinationCoordinate);

        // for example, if the destination is to the South-East, we check South and East first, then the Pause,
        // and then the other directions
        for (Instruction instruction : instructionsInOrder) {
            Coordinate targetCoordinate = instruction.execute(startCoordinate);
            int nextTime = timeElapsed + 1;

            // If the target coordinate is feasible
            if (isCoordinateOnTheGrid(targetCoordinate) &&
                    isCoordinateANewOne(targetCoordinate) &&
                    isCoordinateBugFree(targetCoordinate, nextTime) &&
                    isCoordinateHoleFree(targetCoordinate)) {

                // we add the instruction to the list and go forward
                instructionList.add(instruction);

                // we note the target coordinate, so we won't cross it again
                positionsWeHaveAlreadyBeen.add(targetCoordinate);

                // if found a way in time and without any accidents
                if (findTheWay(instructionList, targetCoordinate, destinationCoordinate, nextTime)) {
                    return true;
                } else {
                    // this way causes trouble - contains a bug, a hole or takes too much time, we undo
                    positionsWeHaveAlreadyBeen.remove(targetCoordinate);
                    instructionList.removeLast();
                }
            }
        }
        return false;
    }

    /**
     * Checks if the coordinate has not been visited already.
     *
     * @param coordinate to be checked
     * @return true if we haven't been there yet
     */
    protected boolean isCoordinateANewOne(Coordinate coordinate) {
        return !positionsWeHaveAlreadyBeen.contains(coordinate);
    }
}

