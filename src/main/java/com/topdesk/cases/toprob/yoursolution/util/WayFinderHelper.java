package com.topdesk.cases.toprob.yoursolution.util;

import com.topdesk.cases.toprob.Coordinate;
import com.topdesk.cases.toprob.Instruction;

import java.util.ArrayList;
import java.util.List;

class WayFinderHelper {

    /**
     * Hiding the default constructor
     */
    private WayFinderHelper() {
    }

    /**
     * Returns a list of {@link Instruction}, with optimal directions first.
     * It will return all possible directions.
     *
     * @param startCoordinate       the current {@link Coordinate}
     * @param destinationCoordinate the destination {@link Coordinate}
     * @return list of {@link Instruction} ordered optimally
     */
    static List<Instruction> getInstructionsInOrder(Coordinate startCoordinate,
                                                    Coordinate destinationCoordinate) {
        List<Instruction> instructionsInOrder = new ArrayList<>();

        // First we add the optimal directions
        // For example, if the destination is to East, we add East first
        if (isDestinationToTheEast(startCoordinate, destinationCoordinate)) {
            instructionsInOrder.add(Instruction.EAST);
        }
        if (isDestinationToTheWest(startCoordinate, destinationCoordinate)) {
            instructionsInOrder.add(Instruction.WEST);
        }
        if (isDestinationToTheSouth(startCoordinate, destinationCoordinate)) {
            instructionsInOrder.add(Instruction.SOUTH);
        }
        if (isDestinationToTheNorth(startCoordinate, destinationCoordinate)) {
            instructionsInOrder.add(Instruction.NORTH);
        }
        // Waiting for the bug to go away is more optimal than going around
        instructionsInOrder.add(Instruction.PAUSE);

        // Adding other directions
        for (Instruction instruction : Instruction.values()) {
            if (!instructionsInOrder.contains(instruction)) {
                instructionsInOrder.add(instruction);
            }
        }
        return instructionsInOrder;
    }

    /**
     * Checks two coordinates, if the destination is to the East
     *
     * @param startCoordinate       current {@link Coordinate}
     * @param destinationCoordinate target {@link Coordinate}
     * @return true if the target is to the East
     */
    private static boolean isDestinationToTheEast(Coordinate startCoordinate, Coordinate destinationCoordinate) {
        return startCoordinate.getX() < destinationCoordinate.getX();
    }

    /**
     * Checks two coordinates, if the destination is to the West
     *
     * @param startCoordinate       current {@link Coordinate}
     * @param destinationCoordinate target {@link Coordinate}
     * @return true if the target is to the West
     */
    private static boolean isDestinationToTheWest(Coordinate startCoordinate, Coordinate destinationCoordinate) {
        return destinationCoordinate.getX() < startCoordinate.getX();
    }

    /**
     * Checks two coordinates, if the destination is to the South
     *
     * @param startCoordinate       current {@link Coordinate}
     * @param destinationCoordinate target {@link Coordinate}
     * @return true if the target is to the South
     */
    private static boolean isDestinationToTheSouth(Coordinate startCoordinate, Coordinate destinationCoordinate) {
        return startCoordinate.getY() < destinationCoordinate.getY();
    }

    /**
     * Checks two coordinates, if the destination is to the North
     *
     * @param startCoordinate       current {@link Coordinate}
     * @param destinationCoordinate target {@link Coordinate}
     * @return true if the target is to the North
     */
    private static boolean isDestinationToTheNorth(Coordinate startCoordinate, Coordinate destinationCoordinate) {
        return destinationCoordinate.getY() < startCoordinate.getY();
    }


}
